{
    'name': 'Sample Module',
    'version': '1.1',
    'author': 'OpenERP SA',
    'category': 'Accounting & Finance',
    'description': """
Accounting and Financial Management.
====================================

Financial and accounting module that covers:
--------------------------------------------
    * General Accounting
    * Cost/Analytic accounting
    * Third party accounting
    * Taxes management
    * Budgets
    * Customer and Supplier Invoices
    * Bank statements
    * Reconciliation process by partner

Creates a dashboard for accountants that includes:
--------------------------------------------------
    * List of Customer Invoices to Approve
    * Company Analysis
    * Graph of Treasury

Processes like maintaining general ledgers are done through the defined Financial Journals (entry move line or grouping is maintained through a journal) 
for a particular financial year and for preparation of vouchers there is a module named account_voucher.
    """,
    'website': 'https://www.odoo.com/page/billing',
    'depends': [],
    'data': [
        'sample_module.xml'
    ],

    'demo': [

    ],
    'test': [

    ],
    'installable': True,
    'auto_install': False,
}