from openerp.osv import fields, osv


class sampleModule(osv.osv):
    _name = "sample.test"
    _columns = {
        'name': fields.char('Name', size=64, required=True, translate=True),
        'title': fields.char('Title', size=64, required=True, translate=True),
        'img': fields.binary('Img')
    }
