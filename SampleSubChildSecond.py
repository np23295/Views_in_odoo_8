from openerp.osv import fields,osv

class sampleSubChildSecond(osv.osv):
    _name="sample.sub1"
    _columns = {
        'state': fields.char('State', size=64, required=True, translate=True),
        'country': fields.char('Country', size=64, required=True, translate=True)
    }