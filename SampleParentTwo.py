from openerp.osv import fields, osv


class SampleParentTwo(osv.osv):
    _name = "sample.sub2.child.2"
    _columns = {
        'phone': fields.integer('Phone', size=10),
        'mobile': fields.integer('Mobile', size=10),
        'date': fields.datetime('Date'),
        'color': fields.many2one('res.users', 'users')
    }
